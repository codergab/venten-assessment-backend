const express = require("express");
const cors = require("cors");

const app = express();

// Require Database
require("./src/database");
// Parse JSON Body
app.use(express.json());
// Import Routes
const routes = require("./src/routes");
app.use(cors());
routes(app);

// Initial App Status
app.get("/", (req, res) => {
  res.send("OK");
});

module.exports = app;
