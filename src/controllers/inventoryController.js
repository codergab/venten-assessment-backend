const axios = require("axios");

const Inventory = require("../models/inventory");

class InventoryController {
  // Get Inventories
  async getInventories(req, res) {
    const inventories = await Inventory.find().limit(10);
    res.send(inventories);
  }

  // Filter Inventories
  async filterInventories(req, res) {
    const payload = req.body;

    const inventories = await Inventory.aggregate([
      {
        $range: [payload.start_date, payload.end_date],
      },
      {
        $match: {
          // start_date: { $gt: payload.start_date },
          // end_date: { $lt: payload.end_date },
          gender: payload.gender ? payload.gender : null,
          country: {
            $in: payload.countries || [],
          },
          car_color: {
            $in: payload.colors || [],
          },
        },
      },
    ]);

    res.send(inventories);
  }

  // Create Inventory
  async createInventory(req, res) {
    const inventory = new Inventory(req.body);

    try {
      await inventory.save();
      res.send(inventory);
    } catch (error) {
      res.status(500).send("Error");
    }
  }

  // Get Filter List
  async getFilterList(req, res) {
    const filters = await axios
      .get("https://ven10.co/assessment/filter.json")
      .then((res) => res.data);
    res.send(filters);
  }
}

const inventory = new InventoryController();

module.exports = inventory;
