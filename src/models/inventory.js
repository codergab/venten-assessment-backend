const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const inventorySchema = new Schema({
  first_name: {
    type: String,
    trim: true,
  },
  last_name: {
    type: String,
    trim: true,
  },
  email: {
    type: String,
    trim: true,
    unique: true,
  },
  gender: {
    type: String,
    enum: ["Male", "Female"],
  },
  job_title: {
    type: String,
  },
  bio: {
    type: String,
  },
  country: {
    type: String,
    trim: true,
  },
  car_model: {
    type: String,
    trim: true,
  },
  car_model_year: {
    type: String,
    trim: true,
  },
  car_color: {
    type: String,
    trim: true,
  },
});

const Inventory = Mongoose.model("Inventory", inventorySchema);

module.exports = Inventory;
