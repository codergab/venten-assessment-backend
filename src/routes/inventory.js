const InventoryController = require("../controllers/inventoryController");

module.exports = (router) => {
  // GET Inventories
  router.get("/inventories", InventoryController.getInventories);
  // Filter Inventory
  router.post("/inventories/filter", InventoryController.filterInventories);
  // Create new Inventory
  router.post("/inventories/create", InventoryController.createInventory);
  // Get Filters List
  router.get("/filter-list", InventoryController.getFilterList);
};
