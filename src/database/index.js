const mongoose = require("mongoose");
const { ENVIRONMENTS } = require("../utils/constants");
console.log(ENVIRONMENTS.DB_URL);
mongoose.connect(
  ENVIRONMENTS.DB_URL,
  {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) throw err;
    console.log("CONNECTED TO DB");
  }
);
