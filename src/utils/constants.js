// Status Codes
const HTTP_STATUS_CODES = {
  SUCCESS: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  SERVER_ERROR: 500,
};

// Env Variables
const ENVIRONMENTS = {
  DB_URL: process.env.DB_URL || "mongodb://127.0.0.1:27017/venten_inventory",
};

module.exports = {
  HTTP_STATUS_CODES,
  ENVIRONMENTS,
};
